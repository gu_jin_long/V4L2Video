#include <fcntl.h>
#include <string.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include "rtmp.h"
#include "x264.h"

#define ENCODE_QUEUE_FRAME_NUM 4

static int fd = 0;
static char *video_dev = "/dev/video9";
static int width = 640;
static int height = 480;
static int frame_rate = 30;
static bool gRunFlag = true;

static unsigned char yuv420pBuffer[1024 * 1024 * 10];
static char *RTMP_URL = "rtmp://192.168.0.123:1935/live/test";
RTMP *rtmp = NULL;
typedef struct buffer
{
    void *start;
    size_t length;
    unsigned int phy_addr;
} VideoBuffer;

static VideoBuffer *videoBuffer;

// 获取当前时间戳（毫秒）
int64_t getCurrentTimestamp()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    int64_t timestamp = (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
    return timestamp;
}


#define FLV_HEADER_SIZE 9
#define FLV_TAG_HEADER_SIZE 11

unsigned char *create_flv_frame(unsigned char *h264_data, int h264_size, long timestamp)
{
    // 计算 FLV 数据大小
    int flv_size = FLV_HEADER_SIZE + FLV_TAG_HEADER_SIZE + h264_size;

    // 分配内存用于存储 FLV 帧数据
    unsigned char *flv_frame = (unsigned char *)malloc(flv_size);
    if (!flv_frame)
    {
        fprintf(stderr, "Failed to allocate memory for FLV frame\n");
        return NULL;
    }

    // 构建 FLV 头部
    flv_frame[0] = 'F';
    flv_frame[1] = 'L';
    flv_frame[2] = 'V';
    flv_frame[3] = 0x01; // Version
    flv_frame[4] = 0x05; // Flags (Audio + Video)
    flv_frame[5] = 0x00;
    flv_frame[6] = 0x00;
    flv_frame[7] = 0x00;
    flv_frame[8] = 0x09; // PreviousTagSize0

    // 构建 FLV Tag 头部
    flv_frame[FLV_HEADER_SIZE] = 0x09;                         // TagType (Video)
    flv_frame[FLV_HEADER_SIZE + 1] = (h264_size >> 16) & 0xFF; // DataSize (高位)
    flv_frame[FLV_HEADER_SIZE + 2] = (h264_size >> 8) & 0xFF;  // DataSize (中位)
    flv_frame[FLV_HEADER_SIZE + 3] = h264_size & 0xFF;         // DataSize (低位)
    flv_frame[FLV_HEADER_SIZE + 4] = (timestamp >> 16) & 0xFF; // Timestamp (高位)
    flv_frame[FLV_HEADER_SIZE + 5] = (timestamp >> 8) & 0xFF;  // Timestamp (中位)
    flv_frame[FLV_HEADER_SIZE + 6] = timestamp & 0xFF;         // Timestamp (低位)
    flv_frame[FLV_HEADER_SIZE + 7] = (timestamp >> 24) & 0xFF; // TimestampExtended
    flv_frame[FLV_HEADER_SIZE + 8] = 0x00;                     // StreamID

    // 复制 H264 数据到 FLV 数据
    memcpy(flv_frame + FLV_HEADER_SIZE + FLV_TAG_HEADER_SIZE, h264_data, h264_size);

    return flv_frame;
}


// 初始化RTMP连接
int rtmp_init()
{
    // 创建一个RTMP对象
    rtmp = RTMP_Alloc();
    if (!rtmp)
    {
        fprintf(stderr, "Failed to allocate RTMP object\n");
        return 0;
    }

    // 初始化RTMP连接
    RTMP_Init(rtmp);

    // 设置连接超时时间
    RTMP_SetupURL(rtmp, RTMP_URL);
    RTMP_EnableWrite(rtmp);
    if (!RTMP_Connect(rtmp, NULL))
    {
        fprintf(stderr, "Failed to connect RTMP server\n");
        return 0;
    }

    if (!RTMP_ConnectStream(rtmp, 0))
    {
        fprintf(stderr, "Failed to connect RTMP stream\n");
        return 0;
    }

    return 1;
}

// 关闭RTMP连接
void rtmp_deinit()
{
    if (rtmp)
    {
        RTMP_Close(rtmp);
        RTMP_Free(rtmp);
        rtmp = NULL;
    }
}

// 推送一帧视频数据
int rtmp_push_frame(unsigned char *data, int size)
{
    if (!rtmp)
    {
        fprintf(stderr, "RTMP connection not initialized\n");
        return 0;
    }

    // 创建一个RTMPPacket对象
    RTMPPacket packet;
    RTMPPacket_Reset(&packet);

    packet.m_nBodySize = size;
    // packet.m_nTimeStamp = RTMP_GetTime();
    packet.m_nTimeStamp = getCurrentTimestamp();
    packet.m_nChannel = 0x04; // Video Channel
    packet.m_packetType = RTMP_PACKET_TYPE_VIDEO;
    packet.m_headerType = RTMP_PACKET_SIZE_LARGE;
    packet.m_nInfoField2 = rtmp->m_stream_id;

    packet.m_body = data;

    // 发送数据
    if (!RTMP_SendPacket(rtmp, &packet, TRUE))
    {
        fprintf(stderr, "Failed to send video frame\n");
        return 0;
    }

    return 1;
}

#if 0
void convertYUYVtoYUV420P(unsigned char *yuyvBuffer, unsigned char *yuv420pBuffer, int width, int height) {
    int frameSize = width * height * 2; // YUYV format has 2 bytes per pixel
    int yIndex = 0;
    int uvIndex = width * height;
    int i, j;

    // Copy Y values as is
    memcpy(yuv420pBuffer, yuyvBuffer, width * height);

    // Interpolate U and V values
    for (i = 0; i < height; i += 2) {
        for (j = 0; j < width; j += 2) {
            // U component (Cb)
            yuv420pBuffer[uvIndex++] = (yuyvBuffer[frameSize + i * width + j * 2] +
                                         yuyvBuffer[frameSize + i * width + (j + 1) * 2]) / 2;
            // V component (Cr)
            yuv420pBuffer[uvIndex++] = (yuyvBuffer[frameSize + i * width + j * 2 + 1] +
                                         yuyvBuffer[frameSize + i * width + (j + 1) * 2 + 1]) / 2;
        }
    }
}
#else
void convertYUYVtoYUV420P(unsigned char *yuyvBuffer, unsigned char *yuv420pBuffer, int width, int height)
{
    int yIndex = 0;
    int uvIndex = width * height;
    int i, j;

    // 转换Y分量（亮度）
    for (i = 0; i < height; ++i)
    {
        for (j = 0; j < width; ++j)
        {
            yuv420pBuffer[yIndex++] = yuyvBuffer[i * width * 2 + j * 2];
        }
    }

    // 转换U和V分量（色度）
    for (i = 0; i < height; i += 2)
    {
        for (j = 0; j < width; j += 2)
        {
            // U分量（Cb）
            yuv420pBuffer[uvIndex++] = (yuyvBuffer[(i * width + j) * 2 + 1] +
                                        yuyvBuffer[((i + 1) * width + j) * 2 + 1] +
                                        yuyvBuffer[(i * width + j + 1) * 2 + 1] +
                                        yuyvBuffer[((i + 1) * width + j + 1) * 2 + 1]) /
                                       4;
            // V分量（Cr）
            yuv420pBuffer[uvIndex++] = (yuyvBuffer[(i * width + j) * 2 + 3] +
                                        yuyvBuffer[((i + 1) * width + j) * 2 + 3] +
                                        yuyvBuffer[(i * width + j + 1) * 2 + 3] +
                                        yuyvBuffer[((i + 1) * width + j + 1) * 2 + 3]) /
                                       4;
        }
    }
}

#endif

void signal_handler(int sig)
{
    printf("recv signal %d\n", sig);
    gRunFlag = false;
}

int getVideoInit()
{
    fd = open(video_dev, O_RDWR);
    if (fd == -1)
    {
        perror("Error opening video device");
        return EXIT_FAILURE;
    }

    // 设置视频格式
    struct v4l2_format format;
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (ioctl(fd, VIDIOC_G_FMT, &format) == -1)
    {
        perror("Error getting format");
        close(fd);
        return EXIT_FAILURE;
    }

    format.fmt.pix.width = width;                   // 设置宽度
    format.fmt.pix.height = height;                 // 设置高度
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV; // 设置像素格式
    if (ioctl(fd, VIDIOC_S_FMT, &format) == -1)
    {
        perror("Error setting format");
        close(fd);
        return EXIT_FAILURE;
    }

    // 设置帧率
    struct v4l2_streamparm fps;
    fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_G_PARM, &fps) == -1)
    {
        perror("Error getting frame rate");
        close(fd);
        return EXIT_FAILURE;
    }

    fps.parm.capture.timeperframe.numerator = 1;            // 设置帧率分子
    fps.parm.capture.timeperframe.denominator = frame_rate; // 设置帧率分母
    if (ioctl(fd, VIDIOC_S_PARM, &fps) == -1)
    {
        perror("Error setting frame rate");
        close(fd);
        return EXIT_FAILURE;
    }

    // 请求分配内存缓冲区
    struct v4l2_requestbuffers reqbuf;
    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
    reqbuf.memory = V4L2_MEMORY_MMAP,
    reqbuf.count = ENCODE_QUEUE_FRAME_NUM;
    if (ioctl(fd, VIDIOC_REQBUFS, &reqbuf) == -1)
    {
        perror("Error requesting buffers");
        close(fd);
        return EXIT_FAILURE;
    }

    // 映射缓冲区
    videoBuffer = (VideoBuffer *)malloc(sizeof(VideoBuffer) * reqbuf.count);
    if (videoBuffer == NULL)
    {
        perror("Error allocating memory for video buffers");
        close(fd);
        return EXIT_FAILURE;
    }

    for (int i = 0; i < reqbuf.count; ++i)
    {
        struct v4l2_buffer buffer;
        buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
        buffer.memory = V4L2_MEMORY_MMAP,
        buffer.index = i;

        if (ioctl(fd, VIDIOC_QUERYBUF, &buffer) == -1)
        {
            perror("Error querying buffer");
            close(fd);
            free(videoBuffer); // Free allocated memory before returning
            return EXIT_FAILURE;
        }

        videoBuffer[i].length = buffer.length;
        videoBuffer[i].start = (unsigned char *)mmap(NULL, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buffer.m.offset);
        if (videoBuffer[i].start == MAP_FAILED)
        {
            perror("Error mapping buffer");
            close(fd);
            free(videoBuffer); // Free allocated memory before returning
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int getVideoRelease()
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    // 停止视频流
    if (ioctl(fd, VIDIOC_STREAMOFF, &type) == -1)
    {
        perror("Error stopping video stream");
        close(fd);
        return EXIT_FAILURE;
    }

    // 清理
    for (int i = 0; i < ENCODE_QUEUE_FRAME_NUM; ++i)
    {
        munmap(videoBuffer[i].start, videoBuffer[i].length);
    }

    if (NULL != videoBuffer)
    {
        free(videoBuffer);
    }

    close(fd);
    return 0;
}

void *frameProcessThread(void *arg)
{
    // 计算帧之间的时间间隔
    struct timespec delay;
    delay.tv_sec = 0;
    delay.tv_nsec = 1000000000 / frame_rate; // 1秒 = 10^9纳秒，所以1000000000 / frame_rate 即为每帧之间的纳秒数vs

    // 开始视频流
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMON, &type) == -1)
    {
        perror("Error starting video stream");
        close(fd);
        return NULL;
    }
    // 初始化编码器参数
    x264_param_t pParam;
    x264_param_default_preset(&pParam, "medium", NULL);
    x264_param_default(&pParam);

    pParam.i_width = width;
    pParam.i_height = height;
    pParam.i_csp = X264_CSP_I420; // YUV 4:2:0 planar
    pParam.rc.b_mb_tree = 0;      // 为0可降低实时编码时的延迟
    pParam.i_slice_max_size = 1024;
    pParam.b_vfr_input = 0;
    pParam.i_fps_num = 30;
    pParam.i_fps_den = 1;
    x264_param_apply_profile(&pParam, "baseline");

    // 分配编码器输入图像
    x264_picture_t pic_in;
    x264_picture_alloc(&pic_in, pParam.i_csp, pParam.i_width, pParam.i_height);

    // 打开编码器
    x264_t *encoder = x264_encoder_open(&pParam);
    if (!encoder)
    {
        printf("Error: Failed to open x264 encoder\n");
        x264_picture_clean(&pic_in);
        close(fd);
        return NULL;
    }

    int64_t i_pts = getCurrentTimestamp(); // 时间戳

    // 循环捕获图像并处理
    while (gRunFlag)
    {
        // 捕获一帧
        struct v4l2_buffer buffer;
        buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
        buffer.memory = V4L2_MEMORY_MMAP;
        if (ioctl(fd, VIDIOC_QBUF, &buffer) == -1)
        {
            perror("Error queuing buffer");
            close(fd);
            return NULL;
        }

        if (ioctl(fd, VIDIOC_DQBUF, &buffer) == -1)
        {
            perror("Error dequeuing buffer");
            close(fd);
            return NULL;
        }

        if (buffer.index >= ENCODE_QUEUE_FRAME_NUM)
        {
            printf("Error: Buffer index out of range\n");
            continue;
        }
        // 获取 YUYV 数据的起始地址
        unsigned char *yuyvBuffer = (unsigned char *)videoBuffer[buffer.index].start;

        // 转换 YUYV 到 YUV420P
        convertYUYVtoYUV420P(yuyvBuffer, yuv420pBuffer, width, height);

        // 填充编码器输入图像数据
        memcpy(pic_in.img.plane[0], yuv420pBuffer, width * height);
        memcpy(pic_in.img.plane[1], yuv420pBuffer + width * height, width * height / 4);
        memcpy(pic_in.img.plane[2], yuv420pBuffer + width * height + width * height / 4, width * height / 4);

        // 编码图像
        i_pts = getCurrentTimestamp(); // 时间戳
        pic_in.i_pts = i_pts;
        x264_nal_t *nal;
        int i_nal;
        x264_picture_t pic_out; // 编码后输出帧
        printf("Encoding frame %lu\n", i_pts);
        long frame_size = x264_encoder_encode(encoder, &nal, &i_nal, &pic_in, &pic_out);
        printf("Encoded frame %lu, frame_size %lu\n", i_pts, frame_size);
        if (frame_size < 0)
        {
            printf("Error: x264_encoder_encode error! frame_size = %lu\n", frame_size);
            continue;
        }
        // 检查编码后的数据是否有效
        if (nal && i_nal > 0)
        {
            // 计算编码后的数据总大小
            size_t total_size = 0;
            for (int i = 0; i < i_nal; ++i)
            {
                total_size += nal[i].i_payload;
            }

            // 分配缓冲区用于存储编码后的数据
            uint8_t *encoded_data = (uint8_t *)malloc(total_size);
            if (!encoded_data)
            {
                printf("Error: Failed to allocate memory for encoded data\n");
                return NULL; // 或者执行其他错误处理操作
            }

            // 将编码后的数据从 nal 拷贝到缓冲区中
            uint8_t *ptr = encoded_data;
            for (int i = 0; i < i_nal; ++i)
            {
                memcpy(ptr, nal[i].p_payload, nal[i].i_payload);
                ptr += nal[i].i_payload;
            }
            int flv_size = FLV_HEADER_SIZE + FLV_TAG_HEADER_SIZE + total_size;
            
            unsigned char *flv_data = create_flv_frame(encoded_data, total_size, i_pts);
            // 推送编码后的数据到 RTMP 服务器
            if (!rtmp_push_frame(flv_data, flv_size))
            {
                fprintf(stderr, "Failed to push video frame\n");
                rtmp_deinit();
                return 1;
            }
            // 注意：在使用完编码后的数据后，记得释放分配的内存
            free(encoded_data);
            free(flv_data);
        }
        else
        {
            printf("Error: No encoded data available\n");
        }

        nanosleep(&delay, NULL);
    }
    // 释放资源
    x264_picture_clean(&pic_in);
    x264_encoder_close(encoder);
    return NULL;
}

int main(int argc, char const *argv[])
{

    signal(SIGINT, signal_handler);

    int ret = getVideoInit();
    if (EXIT_FAILURE == ret)
    {
        perror("init error!\n");
        return -1;
    }
    if (!rtmp_init())
    {
        fprintf(stderr, "Failed to initialize RTMP\n");
        return -1;
    }

    pthread_t tid;
    pthread_create(&tid, NULL, frameProcessThread, NULL);

    while (gRunFlag)
    {
        usleep(100);
    }
    sleep(1);
    getVideoRelease();
    // 关闭RTMP连接
    rtmp_deinit();
    return 0;
}
