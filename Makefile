CC = arm-none-linux-gnueabi-gcc
# CFLAGS := -Wall -std=c++11
CFLAGS := -Wall -std=c11 -O2 -g
CFLAGS += -I./include -I./include/rtmp -I./include/x264 \
		-I./include/openssl -I./include/zlib \

LDFLAGS := -lpthread -ldl -lrt 
LDFLAGS += -L./lib -lrtmp -lx264 -lcJSON
LDFLAGS += -L./lib/openssl -lssl -lcrypto
LDFLAGS += -L./lib/zlib -lz


SRC = v4l2_test.c
OBJ = $(SRC:.c=.o)
EXECUTABLE = video

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJ) $(EXECUTABLE)
